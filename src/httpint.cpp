/**
	This file is a part of the rcw project
	Copyright (C) 2021 rexy712

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Affero General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU Affero General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "httpint.hpp"

namespace rcw{

	httpint::httpint(long x):
		m_code(x){}
	httpint& httpint::operator=(long x){
		m_code = x;
		return *this;
	}

	bool httpint::ok(void)const{
		return (m_code >= 200 && m_code < 300);
	}
	httpint::operator long(void)const{
		return m_code;
	}
	long httpint::get(void)const{
		return m_code;
	}

}
