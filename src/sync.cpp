/**
	This file is a part of the rcw project
	Copyright (C) 2021 rexy712

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Affero General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU Affero General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "sync.hpp"

#include "curl_header_list.hpp"
#include <curl/curl.h>
#include <cstdlib> //size_t
#include <utility> //move
#include <rexy/utility.hpp> //min
#include <cstring> //memcpy, strlen

namespace rcw{

	namespace detail{

		response get_impl(curl_easy_handle& c, curl_cback_invoker* cback, const url& u, const curl_header_list& headers){
			c.prep_get();
			c.set_write_fun(curl_write_callback);
			c.set_header(headers);
			c.set_read_data(nullptr);
			c.set_write_data(cback);
			c.set_url(u.data.data());
			return c.perform();
		}
		response post_impl(curl_easy_handle& c, curl_cback_invoker* read_cback, curl_cback_invoker* write_cback, const url& u, const curl_header_list& headers){
			c.prep_post();
			c.set_write_fun(curl_write_callback);
			c.set_read_fun(curl_read_callback);
			c.set_read_data(read_cback);
			c.set_write_data(write_cback);
			c.set_url(u.data.data());
			c.set_header(headers);
			return c.perform();
		}
		response put_impl(curl_easy_handle& c, curl_cback_invoker* read_cback, curl_cback_invoker* write_cback, const url& u, const curl_header_list& headers){
			c.prep_put();
			c.set_write_fun(curl_write_callback);
			c.set_read_fun(curl_read_callback);
			c.set_read_data(read_cback);
			c.set_write_data(write_cback);
			c.set_url(u.data.data());
			c.set_header(headers);
			return c.perform();
		}
		response del_impl(curl_easy_handle& c, curl_cback_invoker* read_cback, curl_cback_invoker* write_cback, const url& u, const std::initializer_list<header>& headers){
			return del_impl(c, read_cback, write_cback, u, curl_header_list(headers));
		}
		response del_impl(curl_easy_handle& c, curl_cback_invoker* read_cback, curl_cback_invoker* write_cback, const url& u, const curl_header_list& headers){
			c.prep_delete();
			c.set_write_fun(curl_write_callback);
			c.set_read_fun(curl_read_callback);
			c.set_write_data(write_cback);
			c.set_read_data(read_cback);
			c.set_url(u.data.data());
			c.set_header(headers);
			return c.perform();
		}
		response del_impl(curl_easy_handle& c, curl_cback_invoker* write_cback, const url& u, const std::initializer_list<header>& headers){
			return del_impl(c, write_cback, u, curl_header_list(headers));
		}
		response del_impl(curl_easy_handle& c, curl_cback_invoker* write_cback, const url& u, const curl_header_list& headers){
			c.prep_delete();
			c.set_write_fun(curl_write_callback);
			c.set_write_data(write_cback);
			c.set_read_data(nullptr);
			c.set_url(u.data.data());
			c.set_header(headers);
			return c.perform();
		}

	}

	response get(const url& u, std::initializer_list<header> headers){
		curl_easy_handle tmp;
		return get(tmp, u, curl_header_list(headers));
	}
	response get(const url& u, const curl_header_list& headers){
		curl_easy_handle tmp;
		return get(tmp, u, headers);
	}
	response get(curl_easy_handle& c, const url& u, std::initializer_list<header> headers){
		return get(c, u, curl_header_list(headers));
	}
	response get(curl_easy_handle& c, const url& u, const curl_header_list& headers){
		default_curl_write_callback cback{};
		response retval = get(c, cback, u, headers);
		retval.text = std::move(cback.data);
		return retval;
	}

	response post(const url& u, std::initializer_list<header> headers){
		curl_easy_handle tmp;
		return post(tmp, u, curl_header_list(headers));
	}
	response post(const url& u, const curl_header_list& headers){
		curl_easy_handle tmp;
		return post(tmp, u, headers);
	}
	response post(const url& u, const body& b, std::initializer_list<header> headers){
		curl_easy_handle tmp;
		return post(tmp, u, b, curl_header_list(headers));
	}
	response post(const url& u, const body& b, const curl_header_list& headers){
		curl_easy_handle tmp;
		return post(tmp, u, b, headers);
	}
	response post(curl_easy_handle& c, const url& u, std::initializer_list<header> headers){
		return post(c, u, body(""), curl_header_list(headers));
	}
	response post(curl_easy_handle& c, const url& u, const curl_header_list& headers){
		return post(c, u, body(""), headers);
	}
	response post(curl_easy_handle& c, const url& u, const body& b, std::initializer_list<header> headers){
		return post(c, u, b, curl_header_list(headers));
	}
	response post(curl_easy_handle& c, const url& u, const body& b, const curl_header_list& headers){
		default_curl_read_callback rcb{b.data.data(), b.data.length()};
		default_curl_write_callback wcb{};
		response retval = post(c, rcb, wcb, u, headers);
		retval.text = std::move(wcb.data);
		return retval;
	}

	response put(const url& u, std::initializer_list<header> headers){
		curl_easy_handle tmp;
		return put(tmp, u, curl_header_list(headers));
	}
	response put(const url& u, const curl_header_list& headers){
		curl_easy_handle tmp;
		return put(tmp, u, headers);
	}
	response put(curl_easy_handle& c, const url& u, std::initializer_list<header> headers){
		return put(c, u, body(""), curl_header_list(headers));
	}
	response put(curl_easy_handle& c, const url& u, const curl_header_list& headers){
		return put(c, u, body(""), headers);
	}
	response put(const url& u, const body& b, std::initializer_list<header> headers){
		curl_easy_handle tmp;
		return put(tmp, u, b, curl_header_list(headers));
	}
	response put(const url& u, const body& b, const curl_header_list& headers){
		curl_easy_handle tmp;
		return put(tmp, u, b, headers);
	}

	response put(curl_easy_handle& c, const url& u, const body& b, std::initializer_list<header> headers){
		return put(c, u, b, curl_header_list(headers));
	}
	response put(curl_easy_handle& c, const url& u, const body& b, const curl_header_list& headers){
		default_curl_read_callback rcb{b.data.data(), b.data.length()};
		default_curl_write_callback wcb{};
		response retval = put(c, rcb, wcb, u, headers);
		retval.text = std::move(wcb.data);
		return retval;
	}

	response del(const url& u, std::initializer_list<header> headers){
		curl_easy_handle tmp;
		return del(tmp, u, curl_header_list(headers));
	}
	response del(const url& u, const curl_header_list& headers){
		curl_easy_handle tmp;
		return del(tmp, u, headers);
	}
	response del(curl_easy_handle& c, const url& u, std::initializer_list<header> headers){
		return del(c, u, curl_header_list(headers));
	}
	response del(curl_easy_handle& c, const url& u, const curl_header_list& headers){
		default_curl_write_callback wcb{};
		response retval = del(c, wcb, u, headers);
		retval.text = std::move(wcb.data);
		return retval;
	}
	response del(const url& u, const body& b, std::initializer_list<header> headers){
		curl_easy_handle tmp;
		return del(tmp, u, b, curl_header_list(headers));
	}
	response del(const url& u, const body& b, const curl_header_list& headers){
		curl_easy_handle tmp;
		return del(tmp, u, b, headers);
	}
	response del(curl_easy_handle& c, const url& u, const body& b, std::initializer_list<header> headers){
		return del(c, u, b, curl_header_list(headers));
	}
	response del(curl_easy_handle& c, const url& u, const body& b, const curl_header_list& headers){
		default_curl_read_callback rcb{b.data.data(), b.data.length()};
		default_curl_write_callback wcb{};
		response retval = del(c, rcb, wcb, u, headers);
		retval.text = std::move(wcb.data);
		return retval;
	}

}
