/**
	This file is a part of the rcw project
	Copyright (C) 2021 rexy712

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Affero General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU Affero General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "curl_header_list.hpp"

#include <utility> //exchange, swap, move

namespace rcw{

	curl_header_list::curl_header_list(std::initializer_list<header> headers){
		for(auto header : headers){
			(*this) += header.data;
		}
	}
	curl_header_list::curl_header_list(const curl_header_list& c){
		for(const curl_slist* header = c.get();header;header = header->next){
			(*this) += header->data;
		}
	}
	curl_header_list::curl_header_list(curl_header_list&& c)noexcept:
		m_data(std::exchange(c.m_data, nullptr)){}

	curl_header_list::~curl_header_list(void){
		curl_slist_free_all(m_data);
	}

	curl_header_list& curl_header_list::operator=(const curl_header_list& c){
		curl_header_list tmp(c);
		return (*this) = std::move(tmp);
	}
	curl_header_list& curl_header_list::operator=(curl_header_list&& c)noexcept{
		std::swap(m_data, c.m_data);
		return *this;
	}

	curl_header_list& curl_header_list::operator+=(const char* data){
		m_data = curl_slist_append(m_data, data);
		return *this;
	}

	curl_header_list::operator curl_slist*(void){
		return m_data;
	}
	curl_header_list::operator const curl_slist*(void)const{
		return m_data;
	}
	curl_slist* curl_header_list::get(void){
		return m_data;
	}
	const curl_slist* curl_header_list::get(void)const{
		return m_data;
	}

	void curl_header_list::reset(curl_slist* l){
		curl_slist_free_all(m_data);
		m_data = l;
	}



}
