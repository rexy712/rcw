/**
	This file is a part of the rcw project
	Copyright (C) 2021 rexy712

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Affero General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU Affero General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "read_write_cback.hpp"
#include <string> //memcpy

namespace rcw{

	size_t curl_write_callback(char* data, size_t size, size_t nmemb, void* userdata){
		detail::curl_cback_invoker* impl = reinterpret_cast<detail::curl_cback_invoker*>(userdata);
		return (*impl)(data, size, nmemb);
	}
	size_t curl_read_callback(char* data, size_t size, size_t nmemb, void* userdata){
		detail::curl_cback_invoker* impl = reinterpret_cast<detail::curl_cback_invoker*>(userdata);
		return (*impl)(data, size, nmemb);
	}
	size_t default_curl_write_callback::operator()(char* d, size_t size, size_t nmemb){
		data.append(d, size*nmemb);
		return size*nmemb;
	}
	size_t default_curl_read_callback::operator()(char* d, size_t size, size_t nmemb){
		size_t totalsize = size*nmemb;
		size_t remaining = length - offset;
		size_t maxsize = rexy::min(totalsize, remaining);
		if(maxsize)
			memcpy(d, data+offset, maxsize);
		offset += maxsize;
		return maxsize;
	}


}
