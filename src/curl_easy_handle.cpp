/**
	This file is a part of the rcw project
	Copyright (C) 2021 rexy712

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Affero General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU Affero General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "curl_easy_handle.hpp"

#include <utility> //exchange, swap
#include <cstring> //strlen
#include <string>

#include "string.hpp"

namespace rcw{

	curl_easy_handle::curl_easy_handle(void):
		m_curl(curl_easy_init()){}
	curl_easy_handle::curl_easy_handle(const curl_easy_handle& c):
		m_curl(curl_easy_duphandle(c.m_curl)){}
	curl_easy_handle::curl_easy_handle(curl_easy_handle&& c):
		m_curl(std::exchange(c.m_curl, nullptr)){}
	curl_easy_handle::~curl_easy_handle(void){
		curl_easy_cleanup(m_curl);
	}

	curl_easy_handle& curl_easy_handle::operator=(const curl_easy_handle& c){
		return *this = curl_easy_handle(c);
	}
	curl_easy_handle& curl_easy_handle::operator=(curl_easy_handle&& c){
		std::swap(m_curl, c.m_curl);
		return *this;
	}

	curl_easy_handle& curl_easy_handle::prep_put(void){
		setopt(CURLOPT_HTTPGET, 0L);
		setopt(CURLOPT_POST, 0L);
		setopt(CURLOPT_CUSTOMREQUEST, NULL);
		return setopt(CURLOPT_PUT, 1L);
	}
	curl_easy_handle& curl_easy_handle::prep_post(void){
		setopt(CURLOPT_PUT, 0L);
		setopt(CURLOPT_HTTPGET, 0L);
		setopt(CURLOPT_CUSTOMREQUEST, NULL);
		return setopt(CURLOPT_POST, 1L);
	}
	curl_easy_handle& curl_easy_handle::prep_get(void){
		setopt(CURLOPT_PUT, 0L);
		setopt(CURLOPT_POST, 0L);
		setopt(CURLOPT_CUSTOMREQUEST, NULL);
		return setopt(CURLOPT_HTTPGET, 1L);
	}
	curl_easy_handle& curl_easy_handle::prep_delete(void){
		setopt(CURLOPT_PUT, 0L);
		setopt(CURLOPT_POST, 0L);
		setopt(CURLOPT_HTTPGET, 1L);
		return setopt(CURLOPT_CUSTOMREQUEST, "DELETE");
	}
	curl_easy_handle& curl_easy_handle::prep_delete_body(void){
		setopt(CURLOPT_PUT, 0L);
		setopt(CURLOPT_POST, 1L);
		setopt(CURLOPT_HTTPGET, 0L);
		return setopt(CURLOPT_CUSTOMREQUEST, "DELETE");
	}
	curl_easy_handle& curl_easy_handle::set_header(const curl_header_list& headers){
		return setopt(CURLOPT_HTTPHEADER, headers.get());
	}
	curl_easy_handle& curl_easy_handle::set_url(const char* url){
		return setopt(CURLOPT_URL, url);
	}
	curl_easy_handle& curl_easy_handle::set_user_agent(const char* agent){
		return setopt(CURLOPT_USERAGENT, agent);
	}
	curl_easy_handle& curl_easy_handle::set_user_pwd(const char* userpass){
		return setopt(CURLOPT_USERPWD, userpass);
	}
	curl_easy_handle& curl_easy_handle::set_post_data(const char* s, curl_off_t len){
		setopt(CURLOPT_POSTFIELDS, s);
		return setopt(CURLOPT_POSTFIELDSIZE_LARGE, len);
	}
	curl_easy_handle& curl_easy_handle::force_ssl(long version){
		setopt(CURLOPT_SSL_VERIFYPEER, 1);
		setopt(CURLOPT_SSL_VERIFYHOST, 1);
		return setopt(CURLOPT_SSLVERSION, version);
	}
	curl_easy_handle& curl_easy_handle::set_write_fun(write_fun fun){
		return setopt(CURLOPT_WRITEFUNCTION, fun);
	}
	curl_easy_handle& curl_easy_handle::set_write_data(void* data){
		return setopt(CURLOPT_WRITEDATA, data);
	}
	curl_easy_handle& curl_easy_handle::set_read_fun(read_fun fun){
		return setopt(CURLOPT_READFUNCTION, fun);
	}
	curl_easy_handle& curl_easy_handle::set_read_data(void* data){
		return setopt(CURLOPT_READDATA, data);
	}

	curl_easy_handle& curl_easy_handle::set_buffer_size(long size){
		return setopt(CURLOPT_BUFFERSIZE, size);
	}
	curl_easy_handle& curl_easy_handle::set_progress(bool enable){
		return setopt(CURLOPT_NOPROGRESS, (long)(!enable));
	}
	curl_easy_handle& curl_easy_handle::set_max_redirs(long max){
		return setopt(CURLOPT_MAXREDIRS, max);
	}
	curl_easy_handle& curl_easy_handle::set_follow(bool enable){
		return setopt(CURLOPT_FOLLOWLOCATION, (long)enable);
	}
	curl_easy_handle& curl_easy_handle::set_keep_alive(bool enable){
		return setopt(CURLOPT_TCP_KEEPALIVE, (long)enable);
	}

	void curl_easy_handle::reset(void){
		curl_easy_reset(m_curl);
	}
	response curl_easy_handle::perform(void){
		response retval = {};
		CURLcode status = curl_easy_perform(m_curl);
		if(status != CURLE_OK){
			retval.liberror = status;
			return retval;
		}
		curl_off_t x;
		char* y;
		retval.status = get_last_status();
		curl_easy_getinfo(m_curl, CURLINFO_TOTAL_TIME_T, &x);
		retval.elapsed_time_us = x;
		curl_easy_getinfo(m_curl, CURLINFO_SIZE_UPLOAD_T, &x);
		retval.uploaded_bytes = x;
		curl_easy_getinfo(m_curl, CURLINFO_SIZE_DOWNLOAD_T, &x);
		retval.downloaded_bytes = x;
		curl_easy_getinfo(m_curl, CURLINFO_CONTENT_TYPE, &y);
		retval.content_type = rexy::string_view(y);
		return retval;
	}

	std::string curl_easy_handle::escape_to_std(const char* data, int len){
		char* tmp = escape_to_cstr(data, &len, len);
		std::string ret(tmp, len);
		curl_free(tmp);
		return ret;
	}
	char* curl_easy_handle::escape_to_cstr(const char* data, int* outlen, int len){
		if(!len)
			len = strlen(data);
		char* tmp = curl_easy_escape(m_curl, data, len);
		*outlen = strlen(tmp);
		return tmp;
	}

	std::string curl_easy_handle::unescape_to_std(const char* data, int len){
		char* tmp = unescape_to_cstr(data, &len, len);
		std::string ret(tmp, len);
		curl_free(tmp);
		return ret;
	}
	char* curl_easy_handle::unescape_to_cstr(const char* data, int* outlen, int len){
		if(!len)
			len = strlen(data);
		char* tmp = curl_easy_unescape(m_curl, data, len, outlen);
		return tmp;
	}

	string curl_easy_handle::escape(const char* data, int len){
		char* tmp = escape_to_cstr(data, &len, len);
		return string(rexy::steal(tmp), len, len);
	}
	string curl_easy_handle::escape(rexy::string_view data){
		return escape(data.data(), data.length());
	}
	std::string curl_easy_handle::escape_to_std(rexy::string_view data){
		return escape_to_std(data.data(), data.length());
	}
	char* curl_easy_handle::escape_to_cstr(rexy::string_view data, int* outlen){
		return escape_to_cstr(data.data(), outlen, data.length());
	}

	string curl_easy_handle::unescape(const char* data, int len){
		char* tmp = unescape_to_cstr(data, &len, len);
		return string(rexy::steal(tmp), len, len);
	}
	string curl_easy_handle::unescape(rexy::string_view data){
		return unescape(data.data(), data.length());
	}
	std::string curl_easy_handle::unescape_to_std(rexy::string_view data){
		return unescape_to_std(data.data(), data.length());
	}
	char* curl_easy_handle::unescape_to_cstr(rexy::string_view data, int* outlen){
		return escape_to_cstr(data.data(), outlen, data.length());
	}


	long curl_easy_handle::get_last_status(void)const{
		long httpcode;
		curl_easy_getinfo(m_curl, CURLINFO_RESPONSE_CODE, &httpcode);
		return httpcode;
	}
	CURL* curl_easy_handle::get(void){
		return m_curl;
	}
	const CURL* curl_easy_handle::get(void)const{
		return m_curl;
	}
	curl_easy_handle::operator CURL*(void){
		return m_curl;
	}
	curl_easy_handle::operator const CURL*(void)const{
		return m_curl;
	}

}
