/**
	This file is a part of the rcw project
	Copyright (C) 2021 rexy712

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Affero General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU Affero General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "async.hpp"
#include "curl_easy_handle.hpp"
#include "curl_header_list.hpp"
#include "sync.hpp"
#include "string.hpp"

#include <memory> //shared_ptr
#include <future>

namespace rcw{

	std::future<response> async_get(const url& u, std::initializer_list<header> headers){
		return async_get(u, curl_header_list(headers));
	}
	std::future<response> async_get(const url& u, const curl_header_list& headers){
		std::shared_ptr<string> url_copy = std::make_shared<string>(u.data);
		std::shared_ptr<curl_header_list> hlist(new curl_header_list(headers));
		return std::async([=]() -> response
			{
				return get(rcw::url(url_copy->data()), *hlist);
			});

	}

	std::future<response> async_post(const url& u, std::initializer_list<header> headers){
		return async_post(u, body(""), curl_header_list(headers));
	}
	std::future<response> async_post(const url& u, const curl_header_list& headers){
		return async_post(u, body(""), headers);
	}
	std::future<response> async_post(const url& u, const body& b, std::initializer_list<header> headers){
		return async_post(u, b, curl_header_list(headers));
	}
	std::future<response> async_post(const url& u, const body& b, const curl_header_list& headers){
		std::shared_ptr<string> url_copy = std::make_shared<string>(u.data);
		std::shared_ptr<string> body_copy = std::make_shared<string>(b.data);
		std::shared_ptr<curl_header_list> hlist(new curl_header_list(headers));
		return std::async([=]() -> response
			{
				return post(rcw::url(url_copy->data()), rcw::body(body_copy->data()), *hlist);
			});
	}

	std::future<response> async_put(const url& u, std::initializer_list<header> headers){
		return async_put(u, body(""), curl_header_list(headers));
	}
	std::future<response> async_put(const url& u, const curl_header_list& headers){
		return async_put(u, body(""), headers);
	}
	std::future<response> async_put(const url& u, const body& b, std::initializer_list<header> headers){
		return async_put(u, b, curl_header_list(headers));
	}
	std::future<response> async_put(const url& u, const body& b, const curl_header_list& headers){
		std::shared_ptr<string> url_copy = std::make_shared<string>(u.data);
		std::shared_ptr<string> body_copy = std::make_shared<string>(b.data);
		std::shared_ptr<curl_header_list> hlist(new curl_header_list(headers));
		return std::async([=]() -> response
			{
				return put(rcw::url(url_copy->data()), rcw::body(body_copy->data()), *hlist);
			});
	}

	std::future<response> async_del(const url& u, std::initializer_list<header> headers){
		return async_del(u, curl_header_list(headers));
	}
	std::future<response> async_del(const url& u, const curl_header_list& headers){
		std::shared_ptr<string> url_copy = std::make_shared<string>(u.data);
		std::shared_ptr<curl_header_list> hlist(new curl_header_list(headers));
		return std::async([=]() -> response
			{
				return del(rcw::url(url_copy->data()), *hlist);
			});
	}
	std::future<response> async_del(const url& u, const body& b, std::initializer_list<header> headers){
		return async_del(u, b, curl_header_list(headers));
	}
	std::future<response> async_del(const url& u, const body& b, const curl_header_list& headers){
		std::shared_ptr<string> url_copy = std::make_shared<string>(u.data);
		std::shared_ptr<string> body_copy = std::make_shared<string>(b.data);
		std::shared_ptr<curl_header_list> hlist(new curl_header_list(headers));
		return std::async([=]() -> response
			{
				return del(rcw::url(url_copy->data()), rcw::body(body_copy->data()), *hlist);
			});
	}

}
