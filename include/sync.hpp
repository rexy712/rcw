/**
	This file is a part of the rcw project
	Copyright (C) 2021 rexy712

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Affero General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU Affero General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef RCW_SYNC_HPP
#define RCW_SYNC_HPP

#include "string.hpp"
#include "curl_easy_handle.hpp"
#include "httpint.hpp"
#include "response.hpp"
#include "types.hpp"
#include "read_write_cback.hpp"
#include "curl_header_list.hpp"

#include <initializer_list>
#include <utility> //forward
#include <rexy/string.hpp>

namespace rcw{
	namespace detail{

		response get_impl(curl_easy_handle& c, curl_cback_invoker* cback, const url& u, const curl_header_list& headers);
		response post_impl(curl_easy_handle& c, curl_cback_invoker* read_cback, curl_cback_invoker* write_cback, const url& u, const curl_header_list& headers);
		response put_impl(curl_easy_handle& c, curl_cback_invoker* read_cback, curl_cback_invoker* write_cback, const url& u, const curl_header_list& headers);
		response del_impl(curl_easy_handle& c, curl_cback_invoker* read_cback, curl_cback_invoker* write_cback, const url& u, const std::initializer_list<header>& headers);
		response del_impl(curl_easy_handle& c, curl_cback_invoker* read_cback, curl_cback_invoker* write_cback, const url& u, const curl_header_list& headers);
		response del_impl(curl_easy_handle& c, curl_cback_invoker* write_cback, const url& u, const std::initializer_list<header>& headers);
		response del_impl(curl_easy_handle& c, curl_cback_invoker* write_cback, const url& u, const curl_header_list& headers);
	}

	//GET methods
	response get(const url& u, std::initializer_list<header> headers = {});
	response get(curl_easy_handle& c, const url& u, std::initializer_list<header> headers = {});
	response get(const url& u, const curl_header_list& headers);
	response get(curl_easy_handle& c, const url& u, const curl_header_list& headers);
	template<class Func>
	response get(Func&& func, const url& u, std::initializer_list<header> headers = {});
	template<class Func>
	response get(curl_easy_handle& c, Func&& f, const url& u, std::initializer_list<header> headers = {});
	template<class Func>
	response get(Func&& f, const url& u, const curl_header_list& headers);
	template<class Func>
	response get(curl_easy_handle& c, Func&& f, const url& u, const curl_header_list& headers);

	//POST methods
	response post(const url& u, std::initializer_list<header> headers = {});
	response post(const url& u, const curl_header_list& headers);
	response post(const url& u, const body& b, std::initializer_list<header> headers = {});
	response post(const url& u, const body& b, const curl_header_list& headers);
	response post(curl_easy_handle& c, const url& u, std::initializer_list<header> headers = {});
	response post(curl_easy_handle& c, const url& u, const curl_header_list& headers);
	response post(curl_easy_handle& c, const url& u, const body& b, std::initializer_list<header> headers = {});
	response post(curl_easy_handle& c, const url& u, const body& b, const curl_header_list& headers);
	template<class RFunc, class WFunc>
	response post(RFunc&& read_cback, WFunc&& write_cback, const url& u, std::initializer_list<header> headers = {});
	template<class RFunc, class WFunc>
	response post(RFunc&& read_cback, WFunc&& write_cback, const url& u, const curl_header_list& headers);
	template<class RFunc, class WFunc>
	response post(curl_easy_handle& c, RFunc&& read_cback, WFunc&& write_cback, const url& u, std::initializer_list<header> headers = {});
	template<class RFunc, class WFunc>
	response post(curl_easy_handle& c, RFunc&& read_cback, WFunc&& write_cback, const url& u, const curl_header_list& headers);

	//PUT methods
	response put(const url& u, std::initializer_list<header> headers = {});
	response put(const url& u, const curl_header_list& headers);
	response put(curl_easy_handle& c, const url& u, std::initializer_list<header> headers = {});
	response put(curl_easy_handle& c, const url& u, const curl_header_list& headers);
	response put(const url& u, const body& b, std::initializer_list<header> headers = {});
	response put(const url& u, const body& b, const curl_header_list& headers);
	response put(curl_easy_handle& c, const url& u, const body& b, std::initializer_list<header> headers = {});
	response put(curl_easy_handle& c, const url& u, const body& b, const curl_header_list& headers);
	template<class RFunc, class WFunc>
	response put(RFunc&& read_cback, WFunc&& write_cback, const url& u, std::initializer_list<header> headers = {});
	template<class RFunc, class WFunc>
	response put(RFunc&& read_cback, WFunc&& write_cback, const url& u, const curl_header_list& headers);
	template<class RFunc, class WFunc>
	response put(curl_easy_handle& c, RFunc&& read_cback, WFunc&& write_cback, const url& u, std::initializer_list<header> headers = {});
	template<class RFunc, class WFunc>
	response put(curl_easy_handle& c, RFunc&& read_cback, WFunc&& write_cback, const url& u, const curl_header_list& headers);

	//DELETE methods
	response del(const url& u, std::initializer_list<header> headers = {});
	response del(const url& u, const curl_header_list& headers);
	response del(curl_easy_handle& c, const url& u, std::initializer_list<header> headers = {});
	response del(curl_easy_handle& c, const url& u, const curl_header_list& headers);
	response del(const url& u, const body& b, std::initializer_list<header> headers = {});
	response del(const url& u, const body& b, const curl_header_list& headers);
	response del(curl_easy_handle& c, const url& u, const body& b, std::initializer_list<header> headers = {});
	response del(curl_easy_handle& c, const url& u, const body& b, const curl_header_list& headers);
	template<class RFunc, class WFunc>
	response del(RFunc&& read_cback, WFunc&& write_cback, const url& u, std::initializer_list<header> headers = {});
	template<class RFunc, class WFunc>
	response del(RFunc&& read_cback, WFunc&& write_cback, const url& u, const curl_header_list& headers);
	template<class RFunc, class WFunc>
	response del(curl_easy_handle& c, RFunc&& read_cback, WFunc&& write_cback, const url& u, std::initializer_list<header> headers = {});
	template<class RFunc, class WFunc>
	response del(curl_easy_handle& c, RFunc&& read_cback, WFunc&& write_cback, const url& u, const curl_header_list& headers);
	template<class WFunc>
	response del(WFunc&& write_cback, const url& u, std::initializer_list<header> headers = {});
	template<class WFunc>
	response del(WFunc&& write_cback, const url& u, const curl_header_list& headers);
	template<class WFunc>
	response del(curl_easy_handle& c, WFunc&& write_cback, const url& u, std::initializer_list<header> headers = {});
	template<class WFunc>
	response del(curl_easy_handle& c, WFunc&& write_cback, const url& u, const curl_header_list& headers);

	//Implementation of templated GET
	template<class Func>
	response get(Func&& func, const url& u, std::initializer_list<header> headers){
		curl_easy_handle tmp;
		return get(tmp, std::forward<Func>(func), u, headers);
	}
	template<class Func>
	response get(curl_easy_handle& c, Func&& cback_fn, const url& u, std::initializer_list<header> headers){
		detail::curl_cback_invoker_impl cback_runner(std::forward<Func>(cback_fn));
		return detail::get_impl(c, &cback_runner, u, headers);
	}
	template<class Func>
	response get(Func&& f, const url& u, const curl_header_list& headers){
		curl_easy_handle tmp;
		return get(tmp, std::forward<Func>(f), u, headers);
	}
	template<class Func>
	response get(curl_easy_handle& c, Func&& f, const url& u, const curl_header_list& headers){
		detail::curl_cback_invoker_impl cback_runner(std::forward<Func>(f));
		return detail::get_impl(c, &cback_runner, u, headers);
	}

	//Implementation of templated POST
	template<class RFunc, class WFunc>
	response post(RFunc&& read_cback, WFunc&& write_cback, const url& u, std::initializer_list<header> headers){
		curl_easy_handle tmp;
		return post(tmp, std::forward<RFunc>(read_cback), std::forward<WFunc>(write_cback), u, curl_header_list(headers));
	}
	template<class RFunc, class WFunc>
	response post(RFunc&& read_cback, WFunc&& write_cback, const url& u, const curl_header_list& headers){
		curl_easy_handle tmp;
		return post(tmp, std::forward<RFunc>(read_cback), std::forward<WFunc>(write_cback), u, headers);
	}
	template<class RFunc, class WFunc>
	response post(curl_easy_handle& c, RFunc&& read_cback, WFunc&& write_cback, const url& u, std::initializer_list<header> headers){
		return post(c, std::forward<RFunc>(read_cback), std::forward<WFunc>(write_cback), u, curl_header_list(headers));
	}
	template<class RFunc, class WFunc>
	response post(curl_easy_handle& c, RFunc&& read_cback, WFunc&& write_cback, const url& u, const curl_header_list& headers){
		detail::curl_cback_invoker_impl read_runner(std::forward<RFunc>(read_cback));
		detail::curl_cback_invoker_impl write_runner(std::forward<WFunc>(write_cback));
		return detail::post_impl(c, &read_runner, &write_runner, u, headers);
	}

	//Implementation of templated PUT
	template<class RFunc, class WFunc>
	response put(RFunc&& read_cback, WFunc&& write_cback, const url& u, std::initializer_list<header> headers){
		curl_easy_handle tmp;
		return put(tmp, std::forward<RFunc>(read_cback), std::forward<WFunc>(write_cback), u, curl_header_list(headers));
	}
	template<class RFunc, class WFunc>
	response put(RFunc&& read_cback, WFunc&& write_cback, const url& u, const curl_header_list& headers){
		curl_easy_handle tmp;
		return put(tmp, std::forward<RFunc>(read_cback), std::forward<WFunc>(write_cback), u, headers);
	}
	template<class RFunc, class WFunc>
	response put(curl_easy_handle& c, RFunc&& read_cback, WFunc&& write_cback, const url& u, std::initializer_list<header> headers){
		return put(c, std::forward<RFunc>(read_cback), std::forward<WFunc>(write_cback), u, curl_header_list(headers));
	}
	template<class RFunc, class WFunc>
	response put(curl_easy_handle& c, RFunc&& read_cback, WFunc&& write_cback, const url& u, const curl_header_list& headers){
		detail::curl_cback_invoker_impl read_runner(std::forward<RFunc>(read_cback));
		detail::curl_cback_invoker_impl write_runner(std::forward<WFunc>(write_cback));
		return detail::put_impl(c, &read_runner, &write_runner, u, headers);
	}

	//Implementation of templated DELETE
	template<class RFunc, class WFunc>
	response del(RFunc&& read_cback, WFunc&& write_cback, const url& u, std::initializer_list<header> headers){
		curl_easy_handle tmp;
		return del(tmp, std::forward<RFunc>(read_cback), std::forward<WFunc>(write_cback), u, curl_header_list(headers));
	}
	template<class RFunc, class WFunc>
	response del(RFunc&& read_cback, WFunc&& write_cback, const url& u, const curl_header_list& headers){
		curl_easy_handle tmp;
		return del(tmp, std::forward<RFunc>(read_cback), std::forward<WFunc>(write_cback), u, headers);
	}
	template<class RFunc, class WFunc>
	response del(curl_easy_handle& c, RFunc&& read_cback, WFunc&& write_cback, const url& u, std::initializer_list<header> headers){
		return del(c, std::forward<RFunc>(read_cback), std::forward<WFunc>(write_cback), u, curl_header_list(headers));
	}
	template<class RFunc, class WFunc>
	response del(curl_easy_handle& c, RFunc&& read_cback, WFunc&& write_cback, const url& u, const curl_header_list& headers){
		detail::curl_cback_invoker_impl read_runner(std::forward<RFunc>(read_cback));
		detail::curl_cback_invoker_impl write_runner(std::forward<WFunc>(write_cback));
		return detail::del_impl(c, &read_runner, &write_runner, u, headers);
	}
	template<class WFunc>
	response del(WFunc&& write_cback, const url& u, std::initializer_list<header> headers){
		curl_easy_handle tmp;
		return del(tmp, std::forward<WFunc>(write_cback), u, curl_header_list(headers));
	}
	template<class WFunc>
	response del(WFunc&& write_cback, const url& u, const curl_header_list& headers){
		curl_easy_handle tmp;
		return del(tmp, std::forward<WFunc>(write_cback), u, headers);
	}
	template<class WFunc>
	response del(curl_easy_handle& c, WFunc&& write_cback, const url& u, std::initializer_list<header> headers){
		return del(c, std::forward<WFunc>(write_cback), u, curl_header_list(headers));
	}
	template<class WFunc>
	response del(curl_easy_handle& c, WFunc&& write_cback, const url& u, const curl_header_list& headers){
		detail::curl_cback_invoker_impl write_runner(std::forward<WFunc>(write_cback));
		return detail::del_impl(c, &write_runner, u, headers);
	}

}

#endif
