/**
	This file is a part of the rcw project
	Copyright (C) 2021 rexy712

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Affero General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU Affero General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef RCW_ASYNC_HPP
#define RCW_ASYNC_HPP

#include <future>
#include <initializer_list>
#include <memory> //shared_ptr

#include "response.hpp"
#include "types.hpp"
#include "read_write_cback.hpp"
#include "sync.hpp"

namespace rcw{

	//GET methods
	std::future<response> async_get(const url& u, std::initializer_list<header> headers = {});
	std::future<response> async_get(const url& u, const curl_header_list& headers);
	template<class Func>
	std::future<response> async_get(Func&& func, const url& u, std::initializer_list<header> headers = {});
	template<class Func>
	std::future<response> async_get(Func&& func, const url& u, const curl_header_list& headers);

	//POST methods
	std::future<response> async_post(const url& u, std::initializer_list<header> headers = {});
	std::future<response> async_post(const url& u, const curl_header_list& headers);
	std::future<response> async_post(const url& u, const body& b, std::initializer_list<header> headers = {});
	std::future<response> async_post(const url& u, const body& b, const curl_header_list& headers);
	template<class RFunc, class WFunc>
	std::future<response> async_post(RFunc&& rfunc, WFunc&& wfunc, const url& u, std::initializer_list<header> headers = {});
	template<class RFunc, class WFunc>
	std::future<response> async_post(RFunc&& rfunc, WFunc&& wfunc, const url& u, const curl_header_list& headers);

	//PUT methods
	std::future<response> async_put(const url& u, std::initializer_list<header> headers = {});
	std::future<response> async_put(const url& u, const curl_header_list& headers);
	std::future<response> async_put(const url& u, const body& b, std::initializer_list<header> headers = {});
	std::future<response> async_put(const url& u, const body& b, const curl_header_list& headers);
	template<class RFunc, class WFunc>
	std::future<response> async_put(RFunc&& rfunc, WFunc&& wfunc, const url& u, std::initializer_list<header> headers = {});
	template<class RFunc, class WFunc>
	std::future<response> async_put(RFunc&& rfunc, WFunc&& wfunc, const url& u, const curl_header_list& headers);

	//DELETE methods
	std::future<response> async_del(const url& u, std::initializer_list<header> headers = {});
	std::future<response> async_del(const url& u, const curl_header_list& headers);
	std::future<response> async_del(const url& u, const body& b, std::initializer_list<header> headers = {});
	std::future<response> async_del(const url& u, const body& b, const curl_header_list& headers);
	template<class RFunc, class WFunc>
	std::future<response> async_del(RFunc&& rfunc, WFunc&& wfunc, const url& u, std::initializer_list<header> headers = {});
	template<class RFunc, class WFunc>
	std::future<response> async_del(RFunc&& rfunc, WFunc&& wfunc, const url& u, const curl_header_list& headers);
	template<class WFunc>
	std::future<response> async_del(WFunc&& wfunc, const url& u, std::initializer_list<header> headers = {});
	template<class WFunc>
	std::future<response> async_del(WFunc&& wfunc, const url& u, const curl_header_list& headers);



	//Implementation of templated GET
	template<class Func>
	std::future<response> async_get(Func&& func, const url& u, std::initializer_list<header> headers){
		return async_get(std::forward<Func>(func), u, curl_header_list(headers));
	}
	template<class Func>
	std::future<response> async_get(Func&& func, const url& u, const curl_header_list& headers){
		std::shared_ptr<string> url_copy = std::make_shared<string>(u.data);
		std::shared_ptr<detail::curl_cback_invoker> cback(new detail::curl_cback_invoker_impl(std::forward<Func>(func)));
		std::shared_ptr<curl_header_list> hlist(new curl_header_list(headers));
		return std::async([=]() -> response
			{
				return get(*cback, rcw::url(*url_copy), *hlist);
			});
	}

	//Implementation of templated POST
	template<class RFunc, class WFunc>
	std::future<response> async_post(RFunc&& rfunc, WFunc&& wfunc, const url& u, std::initializer_list<header> headers){
		return async_post(std::forward<RFunc>(rfunc), std::forward<WFunc>(wfunc), u, curl_header_list(headers));
	}
	template<class RFunc, class WFunc>
	std::future<response> async_post(RFunc&& rfunc, WFunc&& wfunc, const url& u, const curl_header_list& headers){
		std::shared_ptr<string> url_copy = std::make_shared<string>(u.data);
		std::shared_ptr<detail::curl_cback_invoker> rcback(new detail::curl_cback_invoker_impl(std::forward<RFunc>(rfunc)));
		std::shared_ptr<detail::curl_cback_invoker> wcback(new detail::curl_cback_invoker_impl(std::forward<WFunc>(wfunc)));
		std::shared_ptr<curl_header_list> hlist(new curl_header_list(headers));
		return std::async([=]() -> response
			{
				return post(*rcback, *wcback, rcw::url(*url_copy), *hlist);
			});
	}

	//Implementation of templated PUT
	template<class RFunc, class WFunc>
	std::future<response> async_put(RFunc&& rfunc, WFunc&& wfunc, const url& u, std::initializer_list<header> headers){
		return async_put(std::forward<RFunc>(rfunc), std::forward<WFunc>(wfunc), u, curl_header_list(headers));
	}
	template<class RFunc, class WFunc>
	std::future<response> async_put(RFunc&& rfunc, WFunc&& wfunc, const url& u, const curl_header_list& headers){
		std::shared_ptr<string> url_copy = std::make_shared<string>(u.data);
		std::shared_ptr<detail::curl_cback_invoker> rcback(new detail::curl_cback_invoker_impl(std::forward<RFunc>(rfunc)));
		std::shared_ptr<detail::curl_cback_invoker> wcback(new detail::curl_cback_invoker_impl(std::forward<WFunc>(wfunc)));
		std::shared_ptr<curl_header_list> hlist(new curl_header_list(headers));
		return std::async([=]() -> response
			{
				return put(*rcback, *wcback, rcw::url(*url_copy), *hlist);
			});
	}

	//Implementation of templated DELETE
	template<class RFunc, class WFunc>
	std::future<response> async_del(RFunc&& rfunc, WFunc&& wfunc, const url& u, std::initializer_list<header> headers){
		return async_del(std::forward<RFunc>(rfunc), std::forward<WFunc>(wfunc), u, curl_header_list(headers));
	}
	template<class RFunc, class WFunc>
	std::future<response> async_del(RFunc&& rfunc, WFunc&& wfunc, const url& u, const curl_header_list& headers){
		std::shared_ptr<string> url_copy = std::make_shared<string>(u.data);
		std::shared_ptr<detail::curl_cback_invoker> rcback(new detail::curl_cback_invoker_impl(std::forward<RFunc>(rfunc)));
		std::shared_ptr<detail::curl_cback_invoker> wcback(new detail::curl_cback_invoker_impl(std::forward<WFunc>(wfunc)));
		std::shared_ptr<curl_header_list> hlist(new curl_header_list(headers));
		return std::async([=]() -> response
			{
				return del(*rcback, *wcback, rcw::url(*url_copy), *hlist);
			});
	}
	template<class WFunc>
	std::future<response> async_del(WFunc&& wfunc, const url& u, std::initializer_list<header> headers){
		return async_del(std::forward<WFunc>(wfunc), u, curl_header_list(headers));
	}
	template<class WFunc>
	std::future<response> async_del(WFunc&& wfunc, const url& u, const curl_header_list& headers){
		std::shared_ptr<string> url_copy = std::make_shared<string>(u.data);
		std::shared_ptr<detail::curl_cback_invoker> wcback(new detail::curl_cback_invoker_impl(std::forward<WFunc>(wfunc)));
		std::shared_ptr<curl_header_list> hlist(new curl_header_list(headers));
		return std::async([=]() -> response
			{
				return del(*wcback, rcw::url(*url_copy), *hlist);
			});
	}

}

#endif
