/**
	This file is a part of the rcw project
	Copyright (C) 2021 rexy712

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Affero General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU Affero General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef RCW_RESPONSE_HPP
#define RCW_RESPONSE_HPP

#include "httpint.hpp"
#include "string.hpp"

#include <rexy/string_view.hpp>

namespace rcw{

	class response
	{
	public:
		httpint status;
		size_t elapsed_time_us; //CURLINFO_TOTAL_TIME_T
		size_t uploaded_bytes; //CURLINFO_SIZE_UPLOAD_T
		size_t downloaded_bytes; //CURLINFO_SIZE_DOWNLOAD_T
		rexy::string_view content_type; //CURLINFO_CONTENT_TYPE
		int liberror = 0;

		string text;

		bool ok(void)const;
	};

}

#endif
