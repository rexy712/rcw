/**
	This file is a part of the rcw project
	Copyright (C) 2021 rexy712

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Affero General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU Affero General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef RCW_READ_WRITE_CBACK_HPP
#define RCW_READ_WRITE_CBACK_HPP

#include <cstdlib> //size_t
#include <tuple>
#include <utility> //forward
#include <rexy/meta.hpp> //sequence_tuple
#include "string.hpp"

namespace rcw{

	namespace detail{
		template<class Func, class... Args>
		struct curl_cback_invoker_helper{
			Func&& func;
			std::tuple<Args...> args;

			template<class... Ts, int... Indices>
			size_t operator()(rexy::sequence_tuple<Indices...>, Ts&&... ts){
				return std::forward<Func>(func)(std::forward<Ts>(ts)..., std::get<Indices>(args)...);
			}
		};

		struct curl_cback_invoker{
			virtual ~curl_cback_invoker(void) = default;
			virtual size_t operator()(char* data, size_t size, size_t nmemb) = 0;
		};
		template<class Func, class... Args>
		struct curl_cback_invoker_impl : public curl_cback_invoker{
			curl_cback_invoker_helper<Func, Args...> helper;

			curl_cback_invoker_impl(const curl_cback_invoker_impl&) = default;
			template<class Fn, class... Ts>
			curl_cback_invoker_impl(Fn&& f, Ts&&... ts):
				helper{std::forward<Fn>(f), {std::forward<Ts>(ts)...}}{}
			size_t operator()(char* data, size_t size, size_t nmemb)override{
				return helper(rexy::sequence_gen_t<sizeof...(Args)>{}, data, size, nmemb);
			}
		};
		template<class Func, class... Args>
		curl_cback_invoker_impl(Func&&, Args&&...) -> curl_cback_invoker_impl<Func&&, Args&&...>;
	}

	size_t curl_write_callback(char* data, size_t size, size_t nmemb, void* userdata);
	size_t curl_read_callback(char* data, size_t size, size_t nmemb, void* userdata);
	struct default_curl_write_callback{
		string data;

		size_t operator()(char* d, size_t size, size_t nmemb);
	};
	struct default_curl_read_callback{
		const char* data;
		size_t length;
		size_t offset = 0;

		size_t operator()(char* d, size_t size, size_t nmemb);
	};

}

#endif
