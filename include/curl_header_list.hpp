/**
	This file is a part of the rcw project
	Copyright (C) 2021 rexy712

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Affero General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU Affero General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef RCW_CURL_HEADER_LIST_HPP
#define RCW_CURL_HEADER_LIST_HPP

#include <utility> //forward
#include <curl/curl.h>
#include <initializer_list>

#include "types.hpp"

namespace rcw{

	class curl_header_list
	{
	private:
		curl_slist* m_data = nullptr;

	public:
		curl_header_list(void) = default;
		template<class... Args>
		curl_header_list(Args&&... args);
		curl_header_list(std::initializer_list<header> headers);
		curl_header_list(const curl_header_list&);
		curl_header_list(curl_header_list&&)noexcept;

		~curl_header_list(void);

		curl_header_list& operator=(const curl_header_list&);
		curl_header_list& operator=(curl_header_list&&)noexcept;

		curl_header_list& operator+=(const char* data);

		operator curl_slist*(void);
		operator const curl_slist*(void)const;
		curl_slist* get(void);
		const curl_slist* get(void)const;

		void reset(curl_slist* l = nullptr);

	private:
		template<class T, class... Args>
		void assign(T&& t, Args&&... args);
	};

	template<class... Args>
	curl_header_list::curl_header_list(Args&&... args){
		assign(std::forward<Args>(args)...);
	}
	template<class T, class... Args>
	void curl_header_list::assign(T&& t, Args&&... args){
		(*this) += std::forward<T>(t);
		if constexpr(sizeof...(args) > 0){
			assign(std::forward<Args>(args)...);
		}
	}

}

#endif
