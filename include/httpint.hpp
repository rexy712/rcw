/**
	This file is a part of the rcw project
	Copyright (C) 2021 rexy712

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Affero General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU Affero General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef RCW_HTTPINT_HPP
#define RCW_HTTPINT_HPP

namespace rcw{

	//class representing http status codes where 200-299 is valid and 300+ is invalid
	class httpint
	{
	private:
		long m_code = 200;

	public:
		httpint(void) = default;
		httpint(long x);
		httpint(const httpint&) = default;
		httpint& operator=(const httpint&) = default;
		httpint& operator=(long x);

		bool ok(void)const;
		operator long(void)const;
		long get(void)const;
	};

}

#endif
