/**
	This file is a part of the rcw project
	Copyright (C) 2021 rexy712

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Affero General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU Affero General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef RCW_CURL_EASY_HANDLE_HPP
#define RCW_CURL_EASY_HANDLE_HPP

#include <curl/curl.h>
#include <cstddef> //size_t
#include <utility> //forward
#include <string>

#include "string.hpp"

#include "curl_header_list.hpp"
#include "response.hpp"

namespace rcw{

	class curl_easy_handle
	{
	private:
		CURL* m_curl;

	public:
		using write_fun = size_t(*)(char*,size_t,size_t,void*);
		using read_fun = size_t(*)(char*,size_t,size_t,void*);

	public:
		curl_easy_handle(void);
		curl_easy_handle(const curl_easy_handle&);
		curl_easy_handle(curl_easy_handle&&);
		~curl_easy_handle(void);

		curl_easy_handle& operator=(const curl_easy_handle&);
		curl_easy_handle& operator=(curl_easy_handle&&);

		template<class T>
		curl_easy_handle& setopt(CURLoption option, T&& t);

		curl_easy_handle& prep_put(void);
		curl_easy_handle& prep_post(void);
		curl_easy_handle& prep_get(void);
		curl_easy_handle& prep_delete(void);
		curl_easy_handle& prep_delete_body(void);

		curl_easy_handle& set_header(const curl_header_list&);
		curl_easy_handle& set_url(const char* url);
		curl_easy_handle& set_user_agent(const char* agent);
		curl_easy_handle& set_user_pwd(const char* userpass);
		curl_easy_handle& set_post_data(const char* s, curl_off_t len = -1);
		curl_easy_handle& force_ssl(long version = CURL_SSLVERSION_DEFAULT);
		curl_easy_handle& set_write_fun(write_fun fun);
		curl_easy_handle& set_write_data(void* data);
		curl_easy_handle& set_read_fun(read_fun fun);
		curl_easy_handle& set_read_data(void* data);

		curl_easy_handle& set_buffer_size(long size);
		curl_easy_handle& set_progress(bool enable);
		curl_easy_handle& set_max_redirs(long max);
		curl_easy_handle& set_follow(bool enable);
		curl_easy_handle& set_keep_alive(bool enable);

		void reset(void);
		response perform(void);

		string escape(const char* data, int len = 0);
		string escape(rexy::string_view data);
		std::string escape_to_std(const char* data, int len = 0);
		std::string escape_to_std(rexy::string_view data);
		char* escape_to_cstr(const char* data, int* outlen, int len = 0);
		char* escape_to_cstr(rexy::string_view data, int* outlen);

		std::string unescape_to_std(const char* data, int len = 0);
		std::string unescape_to_std(rexy::string_view data);
		string unescape(const char* data, int len = 0);
		string unescape(rexy::string_view data);
		char* unescape_to_cstr(const char* data, int* outlen, int len = 0);
		char* unescape_to_cstr(rexy::string_view data, int* outlen);

		long get_last_status(void)const;
		CURL* get(void);
		const CURL* get(void)const;
		operator CURL*(void);
		operator const CURL*(void)const;

	};

	template<class T>
	curl_easy_handle& curl_easy_handle::setopt(CURLoption option, T&& t){
		curl_easy_setopt(m_curl, option, std::forward<T>(t));
		return *this;
	}
}

#endif
