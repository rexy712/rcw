#include <cstdio> //printf, fprintf, stderr
#include "rcw.hpp"

#define PRINT_RUNNING_FUNC() fprintf(stderr, "Running %s\n", __func__)

void do_async_get(){
	PRINT_RUNNING_FUNC();
	std::future<rcw::response> f_reply = rcw::async_get(rcw::url("https://httpbin.org/get"));
	rcw::response reply = f_reply.get();
	printf("%ld\n", reply.status.get());
	if(reply.ok())
		printf("%s\n", reply.text.data());
}
void do_async_post(){
	PRINT_RUNNING_FUNC();
	std::future<rcw::response> f_reply = rcw::async_post(rcw::url("https://httpbin.org/post"));
	rcw::response reply = f_reply.get();
	printf("%ld\n", reply.status.get());
	if(reply.ok())
		printf("%s\n", reply.text.data());
}
void do_async_put(){
	PRINT_RUNNING_FUNC();
	std::future<rcw::response> f_reply = rcw::async_put(rcw::url("https://httpbin.org/put"));
	rcw::response reply = f_reply.get();
	printf("%ld\n", reply.status.get());
	if(reply.ok())
		printf("%s\n", reply.text.data());
}
void do_async_del(){
	PRINT_RUNNING_FUNC();
	std::future<rcw::response> f_reply = rcw::async_del(rcw::url("https://httpbin.org/delete"));
	rcw::response reply = f_reply.get();
	printf("%ld\n", reply.status.get());
	if(reply.ok())
		printf("%s\n", reply.text.data());
}

int main(){
	do_async_get();
	do_async_post();
	do_async_put();
	do_async_del();
}
