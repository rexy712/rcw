#include <cstdio> //printf, fprintf, stderr
#include "rcw.hpp"

#define PRINT_RUNNING_FUNC() fprintf(stderr, "Running %s\n", __func__)

void do_get(){
	PRINT_RUNNING_FUNC();
	rcw::response reply = rcw::get(rcw::url("https://httpbin.org/get"));
	printf("%ld\n", reply.status.get());
	if(reply.ok())
		printf("%s\n", reply.text.data());
}

void do_post(){
	PRINT_RUNNING_FUNC();
	rcw::response reply = rcw::post(rcw::url("https://httpbin.org/post"));
	printf("%ld\n", reply.status.get());
	if(reply.ok())
		printf("%s\n", reply.text.data());
}
void do_put(){
	PRINT_RUNNING_FUNC();
	rcw::response reply = rcw::put(rcw::url("https://httpbin.org/put"));
	printf("%ld\n", reply.status.get());
	if(reply.ok())
		printf("%s\n", reply.text.data());
}
void do_del(){
	PRINT_RUNNING_FUNC();
	rcw::response reply = rcw::del(rcw::url("https://httpbin.org/delete"));
	printf("%ld\n", reply.status.get());
	if(reply.ok())
		printf("%s\n", reply.text.data());
}

int main(){
	do_get();
	do_post();
	do_put();
	do_del();
}
